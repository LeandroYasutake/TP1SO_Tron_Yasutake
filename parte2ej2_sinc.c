#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>

sem_t A, B, C, D, AUX;
int repetir;

void* funcA ()
{   
    int repeticiones = repetir;
    while(repeticiones--)
    {
        sem_wait(&A);
        printf("​→");
        sem_post(&B);
    }
    pthread_exit(NULL);
}

void* funcB ()
{
    int repeticiones = repetir*2;
    while(repeticiones--)
    {
        sem_wait(&B);
        printf("​↑");
        sem_post(&AUX);
    }
    pthread_exit(NULL);    
}

void* funcC ()
{
    int repeticiones = repetir;
    while(repeticiones--)
    {
        sem_wait(&C);
        sem_wait(&AUX);
        printf("​←");
        sem_post(&D);
        sem_post(&B);
    }
    pthread_exit(NULL);
}

void* funcD ()
{
    int repeticiones = repetir;
    while(repeticiones--)
    {
        sem_wait(&D);
        sem_wait(&AUX);
        printf("​\n");
        sem_post(&C);
        sem_post(&A);
    }
    pthread_exit(NULL);    
}

void* enterInt()
{
    int n;
    printf("Ingrese el número de repeticiones deseado: ");
    
    if (scanf("%d", &n) != 1)
    {
      printf("Ingreso erróneo. Intente nuevamente!\n");
    }
    else
    {
      repetir = n;
    }
}

void checkSem(int res)
{
    if (res != 0) 
    {
        perror("Inicialización del semáforo ha fallado!");
	    exit(EXIT_FAILURE);
    }
}

void checkThread(int rc)
{
    if (rc)
    {
        printf("Error al crear el thread, %d \n", rc);
        exit(EXIT_FAILURE);
    }
}

int main ()
{
    enterInt();
    int res, rc;
    pthread_t threadA, threadB, threadC, threadD;

    res = sem_init(&A, 0, 1);
    checkSem(res);
    res = sem_init(&B, 0, 0);
    checkSem(res);
    res = sem_init(&C, 0, 1);
    checkSem(res);
    res = sem_init(&D, 0, 0);
    checkSem(res);
    res = sem_init(&AUX, 0, 0);
    checkSem(res);

    printf("Lanzando threads...\n");
    printf("\n");

    rc = pthread_create(&threadB, NULL, funcB, NULL);
    checkThread(rc);
    rc = pthread_create(&threadC, NULL, funcC, NULL);
    checkThread(rc);
    rc = pthread_create(&threadD, NULL, funcD, NULL);
    checkThread(rc);
    rc = pthread_create(&threadA, NULL, funcA, NULL);
    checkThread(rc);
    
    //Threads desordenados
    
    pthread_join(threadA, NULL);
    pthread_join(threadB, NULL);
    pthread_join(threadC, NULL);
    pthread_join(threadD, NULL);

    sem_destroy(&A);
    sem_destroy(&B);
    sem_destroy(&C);
    sem_destroy(&D);
    sem_destroy(&AUX);

    pthread_exit(NULL);
}
