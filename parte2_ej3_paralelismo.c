#include <stdio.h>
#include <stdlib.h>    
#include <unistd.h>     // para castear de puntero a entero
#include <string.h>
#include <time.h>
#include <pthread.h>

#define CANTIDAD_LUGARES 7
char* lugares_labels[] = {"Norte America", "Centro America", "Sud America", "Africa", "Europa", "Asia", "Oceania"};

void esperar () 
{
  //simular un tiempo de ejecucion de algun script araña (algun codigo en python)

  //inicializar en 1 segundo = 1000000 microseconds:
  int microseconds = 1000000;

  //dormir el thread, simula que esta haciendo alguna tarea
  usleep(microseconds);
}


void* recopilar_informacion (void* parametro)
{
    int posArray = (intptr_t) parametro;
    printf("***************************************** \n");
    printf("Buscando informacion en  %s \n", lugares_labels[posArray]);
    esperar();
    pthread_exit(NULL);
}

void checkThread(int rc)
{
    if (rc)
    {
        printf("Error al crear el thread, %d \n", rc);
        exit(EXIT_FAILURE);
    }
}

void checkSem(int res)
{
    if (res != 0) 
    {
        perror("Inicialización del semáforo ha fallado!");
	    exit(EXIT_FAILURE);
    }
}

int main ()
{
    pthread_t threads[CANTIDAD_LUGARES]; 
    int i, rc;

    for (i = 0; i < CANTIDAD_LUGARES; i++)
    {
        rc = pthread_create(&threads[i], NULL, recopilar_informacion, (void *)(intptr_t) i);    
        checkThread(rc);
    }

    for(i = 0 ; i < CANTIDAD_LUGARES ; i++)
    {
        pthread_join(threads[i] , NULL);
    }

    pthread_exit(NULL);
}

/*
    tiempo de ejecucion de este programa secuencial:
    real	0m6.003s
    user	0m0.000s
    sys	    0m0.002s

    tiempo de ejecucion de este programa en paralelo sin semáforos:
    real	0m1.003s
    user	0m0.000s
    sys	    0m0.002s
*/
