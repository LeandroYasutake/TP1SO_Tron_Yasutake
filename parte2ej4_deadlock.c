#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define num_filos 5
sem_t tenedores[num_filos];

void* funcPensarComer(void* parametro)
{
    int filosofo = (intptr_t) parametro;
    int microseconds = filosofo*100000;

    printf ("\nFilósofo %d esta pensando ", filosofo+1);

    sem_wait(&tenedores[filosofo]);

    usleep(microseconds); //simulamos una espera para forzar el deadlock

    sem_wait(&tenedores[ (filosofo+1)%5 ]);

    printf ("\nFilósofo %d esta comiendo ", filosofo+1);

    sem_post(&tenedores[filosofo]);
    sem_post(&tenedores[ (filosofo+1)%5 ]);

    pthread_exit(NULL); 
}

void checkThread(int rc)
{
    if (rc)
    {
        printf("Error al crear el thread, %d \n", rc);
        exit(EXIT_FAILURE);
    }
}

void checkSem(int res)
{
    if (res != 0) 
    {
        perror("Inicialización del semáforo ha fallado!");
	    exit(EXIT_FAILURE);
    }
}

int main ()
{
    pthread_t filosofos[num_filos]; 
    int i, rc, res;

    res = sem_init(&tenedores[0], 0, 1);
    checkSem(res);
    res = sem_init(&tenedores[1], 0, 1);
    checkSem(res);
    res = sem_init(&tenedores[2], 0, 1);
    checkSem(res);
    res = sem_init(&tenedores[3], 0, 1);
    checkSem(res);
    res = sem_init(&tenedores[4], 0, 1);
    checkSem(res);

    for (i = 0; i < num_filos ; i++)
    {
        rc = pthread_create(&filosofos[i], NULL, funcPensarComer, (void *)(intptr_t) i);    
        checkThread(rc);
    }

    for(i = 0 ; i < num_filos ; i++)
    {
        pthread_join(filosofos[i] , NULL);
    }

    for(i = 0 ; i < num_filos ; i++)
    {
        sem_destroy(&tenedores[i]);
    }

    pthread_exit(NULL);
}
