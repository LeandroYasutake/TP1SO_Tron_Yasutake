#!/bin/bash
chmod 744 supermenu.sh
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------
proyectoActual="Trabajo Práctico Nº1 Sistemas Operativos: Tron";
proyectos="/home/alumno/TP1SO_Tron_Yasutake";

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () 
{
    imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver contenido de carpeta";
    echo -e "\t\t\t b.  Ver variables del sistema";
    echo -e "\t\t\t c.  Buscar programa instalado";
    echo -e "\t\t\t d.  Creación de usuario nuevo";        
    echo -e "\t\t\t e.  Buscar actividad del usuario";
    echo -e "\t\t\t f.  Instalación de servidor";
    echo -e "\t\t\t g.  Más opciones...";               
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

imprimir_menu2 () 
{
    imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Más opciones:";
    echo "";
    echo -e "\t\t\t Enunciado 2da Parte: Threads y Semáforos";
    echo "";
    echo -e "\t\t\t h. Ejercicio 1 : Exclusión Mutua";
    echo -e "\t\t\t i. Ejercicio 2 : Sincronización";
    echo -e "\t\t\t j. Ejercicio 3 : El Poder del Paralelismo";
    echo -e "\t\t\t k. Ejercicio 4 : Los Peligros de la Sincronización";
    echo -e "\t\t\t l. Ver Créditos";
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

seleccionar_menu2 () 
{
    while  true
    do
        # 1. mostrar el menu
        imprimir_menu2;
        # 2. leer la opcion del usuario
        read opcion;
        
        case $opcion in
            h|H) h_funcion;;
            i|I) i_funcion;;
            j|J) j_funcion;;
            k|K) k_funcion;;
            l|L) l_funcion;;
            q|Q) break;;
            *) malaEleccion;;
        esac
        esperar;
    done
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () 
{
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () 
{
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () 
{
    echo -e "Selección Inválida ..." ;
}

decidir () 
{
    echo $1;
    while true; 
    do
        echo "desea ejecutar? (s/n)";
        read respuesta;
        case $respuesta in
            [Nn]* ) break;;
            [Ss]* ) eval $1
            break;;
            * ) echo "Por favor tipear S/s ó N/n.";;
        esac
        esperar;
    done
}

#------------------------------------------------------
# EJERCICIOS
#------------------------------------------------------

mostrarVariables() 
{
    echo -n "Kernel: "; uname -r; 
    echo "";
    echo -n "Arquitectura de la PC: "; uname -m; 
    echo "";
    echo -n "CPU: "; lscpu | grep 'Model name'; 
    echo "";
    echo -n -e "Primeras 10 interrupciones: \n"; cat /proc/interrupts | head -11; 
    echo "";
    echo -n -e "Memoria swap: \n"; cat /proc/swaps; 
    echo "";
    echo -n "Cantidad de memoria principal: "; cat /proc/meminfo | grep MemTotal; 
    echo "";
    echo -n "Placa de video: "; lspci | grep VGA; 
    echo "";
    echo -n "Idioma del teclado: "; setxkbmap -query | grep layout; 
}

buscarPrograma()
{
    echo -e "Ingrese el nombre de un programar a buscar \n";
    read programa;
    echo -e "Buscando $programa...\n"
    dpkg -l | grep $programa;
}

crearSudoer()
{
    echo "Creando usuario 'sor1'...":
    if [ ! -d "/home/sor1" ]; then
        sudo mkdir /home/sor1;                          #sudo rm -R /home/sor1
        sudo useradd -g sudo sor1;
        sudo chown sor1 /home/sor1;
        sleep 1;
        sudo passwd sor1;
        sleep 1;
        echo "Accediendo como 'sor1'...":
        echo "";
        sudo su -c 'id' - sor1;                         #cut -d: -f1 /etc/passwd user list
    else
        echo "Usuario ya creado!"   
    fi
}

buscarActividad() 
{
    echo "Buscando actividad del usuario 'sor1'..."
    sleep 1;
    if [ ! -d "/home/RegistroActividad" ]; then
        sudo mkdir /home/RegistroActividad;
        sudo touch /home/RegistroActividad/registrosSor1.txt;
    fi
    sudo sh -c "cat /var/log/auth.log | grep sor1 >> /home/RegistroActividad/registrosSor1.txt";
    vim -e "/home/RegistroActividad/registrosSor1.txt" & 
    echo "Registro abierto en background proceso nº: $!";
    echo "Registros guardados!";
}

instalarServidor()
{
    declare lineaCont=0;
    
    while read -r -u9 linea; 
    do
        if [[ ! -z "$linea" ]] && [[ ! "$linea" == "#"* ]]; then
            echo -e $USER"@"$HOSTNAME":"$PWD"\n";
            echo -n "Linea $lineaCont: ";
            decidir "$linea";
            echo "---------------------------------------------------";
            ((lineaCont++));
        else
            echo $linea;    
        fi
    done 9< /home/alumno/TP1SO_Tron_Yasutake/comandos.txt;
}

lanzarExclusionMutua() 
{
    gcc parte2_ej1_exclu_mutua_Mutex.c -o parte2_ej1_Mutex -lpthread;
    gcc parte2_ej1_exclu_mutua.c -o parte2_ej1_exclu_mutua -lpthread;
    echo "Lanzando threads sin Mutex...";
    echo "";
    ./parte2_ej1_exclu_mutua;
    echo "";
    echo "Lanzando threads con Mutex...";
    echo "";
    ./parte2_ej1_Mutex;
}

lanzarSincronizacion() 
{
    gcc parte2ej2_sinc.c -o parte2ej2_sinc -lpthread;
    echo "";
    ./parte2ej2_sinc;
}

lanzarParalelismo() 
{
    gcc parte2_ej3_paralelismo.c -o ej3Paralelo -lpthread;
    echo "--------  Ejecución en paralelo  --------";
    echo "";
    time ./ej3Paralelo;
    gcc parte2_ej3_paralelismoSecuencial.c -o ej3Secuencial;
    echo "";
    echo "---------- Ejecución secuencial ----------";
    echo "";
    time ./ej3Secuencial;
}

lanzarDeadlock() 
{
    gcc parte2ej4_deadlock.c -o ej4deadlock -lpthread;
    echo "Generando deadlock!";
    echo "";
    ./ej4deadlock;
}

lanzarCreditos() 
{
    echo "";
    echo "Sistemas Operativos y Redes: Trabajo Práctico 1";
    echo "";
    echo "Alumno: Leandro Yasutake";
    echo "";
    echo "Docentes: Andrés Rojas Paredes - autaro Tacchini";
    echo "";
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () 
{
        imprimir_encabezado "\tOpción a";
        decidir "ls -l";
}

b_funcion () 
{
        imprimir_encabezado "\tOpción b";
        mostrarVariables;
}

c_funcion () 
{
        imprimir_encabezado "\tOpción c";
        buscarPrograma;      
}

d_funcion () 
{
        imprimir_encabezado "\tOpción d";
        crearSudoer;
}

e_funcion () 
{
        imprimir_encabezado "\tOpción e";        
        buscarActividad;
}

f_funcion () 
{
        imprimir_encabezado "\tOpción f";
        instalarServidor;        
}

g_funcion () 
{
        imprimir_encabezado "\tOpción g";
        seleccionar_menu2;
}

h_funcion () 
{
        imprimir_encabezado "\tOpción h";
        lanzarExclusionMutua;
}

i_funcion () 
{
        imprimir_encabezado "\tOpción i";
        lanzarSincronizacion;
}

j_funcion () 
{
        imprimir_encabezado "\tOpción j";
        lanzarParalelismo;
}

k_funcion () 
{
        imprimir_encabezado "\tOpción k";
        lanzarDeadlock;
}

l_funcion () 
{
        imprimir_encabezado "\tOpción l";
        lanzarCreditos;
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
